How to build and run
===

```
$ mvn clean package -Dmaven.test.skip=true assembly:single
$ java -jar target/myapp-full.jar producer-config.txt producer
$ java -jar target/myapp-full.jar consumer-config.txt consumer
```
