package com.example.myapp;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.clients.consumer.*;
import java.time.Duration;
import java.io.*;
import java.nio.file.*;
import java.util.*;


public class App
{
    static String TOPIC = "uly.foobar.new";

    public static void main(final String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("Please provide the configuration file path as a command line argument");
            System.exit(1);
        }

        // Load producer configuration settings from a local file
        final Properties props = loadConfig(args[0]);
        final String action = args[1];

        switch(action) {
            case "producer":
                produceData(props);
                break;
            case "consumer":
                consumeData(props);
                break;
            default:
                System.err.println("It must be producer or consumer");
                System.exit(1);
        }
    }

    private static void produceData(Properties props) throws IOException {
        String[] users = { "eabara", "jsmith", "sgarcia", "jbernard", "htanaka", "awalther" };
        String[] items = { "book", "alarm clock", "t-shirts", "gift card", "batteries" };
        Producer<String, String> producer = new KafkaProducer<>(props);

        final Long numMessages = 10L;
        for (Long i = 0L; i < numMessages; i++) {
            Random rnd = new Random();
            String user = users[rnd.nextInt(users.length)];
            String item = items[rnd.nextInt(items.length)];

            producer.send(
              new ProducerRecord<>(TOPIC, user, item),
              (event, ex) -> {
                  if (ex != null)
                      ex.printStackTrace();
                  else
                      System.out.printf("Produced event to topic %s: key = %-10s value = %s%n", TOPIC, user, item);
              });
        }

        producer.flush();
        System.out.printf("%s events were produced to topic %s%n", numMessages, TOPIC);
        producer.close();
    }

    private static void consumeData(Properties props) throws IOException {
        // Add additional properties.
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "kafka-java-getting-started");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // Add additional required properties for this consumer app
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(TOPIC));

        try {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, String> record : records) {
                    String key = record.key();
                    String value = record.value();
                    System.out.println(
                        String.format("Consumed event from topic %s: key = %-10s value = %s", TOPIC, key, value));
                }
            }
        } finally {
            consumer.close();
        }
    }

    public static Properties loadConfig(final String configFile) throws IOException {
        if (!Files.exists(Paths.get(configFile))) {
            throw new IOException(configFile + " not found.");
        }
        final Properties cfg = new Properties();
        try (InputStream inputStream = new FileInputStream(configFile)) {
            cfg.load(inputStream);
        }
        return cfg;
    }
}
